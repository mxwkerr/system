;; Time Startup Performance AndImprove By Disabling Garage Collectio
;; The default is 800 kilobytes.  Measured in bytes. Reset to default
;; At The End
(setq gc-cons-threshold (* 50 1000 1000))

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

(require `package)                     
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)

(unless package-archive-contents       
  (package-refresh-contents))         

(unless (package-installed-p 'use-package)
  (package-install `use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package quelpa)

(use-package geiser :ensure t)
(use-package geiser-guile)

(setq inhibit-startup-message t)

(scroll-bar-mode -1)
(tool-bar-mode -1)			
(tooltip-mode -1)			
(menu-bar-mode -1)

(setq visible-bell 1)

(set-frame-parameter (selected-frame) 'alpha '(92 . 92))
(add-to-list 'default-frame-alist '(alpha . (92 . 92)))

(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(set-face-attribute 'default nil :font "-UKWN-Iosevka Fixed-normal-normal-normal-*-21-*-*-*-m-0-iso10646-1")
(set-face-attribute 'fixed-pitch nil :font "-UKWN-Iosevka Term-normal-normal-normal-*-21-*-*-*-d-0-iso10646-1")
(set-face-attribute 'variable-pitch nil :font "-bB  -Fira Sans-light-normal-normal-*-21-*-*-*-*-0-iso10646-1")

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package buffer-flip
:ensure t
:bind  (("M-<tab>" . buffer-flip)
        :map buffer-flip-map
        ( "M-<tab>" .   buffer-flip-forward) 
        ( "M-S-<tab>" . buffer-flip-backward) 
        ( "M-ESC" .     buffer-flip-abort))
:config
(setq buffer-flip-skip-patterns
      '("^\\*helm\\b"
        "^\\*swiper\\*$")))

(defun setup/org-mode-setup ()
   (org-indent-mode)
   (visual-line-mode 1)
   (variable-pitch-mode 1)

   (set-face-attribute 'org-document-title nil :font "Cantarell" :weight 'bold :height 1.3)
;; Set font sizes for different heading levels
(dolist (face '((org-level-1 . 1.4)
                 (org-level-2 . 1.2)
                 (org-level-3 . 1.1)
                 (org-level-4 . 1.0)
                 (org-level-5 . 1.0)
                 (org-level-6 . 1.0)
                 (org-level-7 . 1.0)
                 (org-level-8 . 1.0)))
   (set-face-attribute (car face) nil :font "Cantarell" :weight 'medium :height (cdr face)))

 ;(set-face-attribute 'org-block nil    :foreground nil :inherit 'variable-pitch)
 (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
 (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
 (set-face-attribute 'org-code nil     :inherit 'fixed-pitch)
 (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
 (set-face-attribute 'org-verbatim nil :inherit '(shadow variable-pitch))
 (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
 (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
 (set-face-attribute 'org-checkbox nil  :inherit 'variable-pitch)
 (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
 (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . setup/org-mode-setup)
  :config
  (setq org-hide-emphasis-markers t         ;make font markup invisible
        org-src-fontify-natively t          ;make src block look nice
        org-fontify-quote-and-verse-blocks t ;make quotes and verse look nice
        org-src-tab-acts-natively t         ;TAB behaves as in src block mode
        org-edit-src-content-indentation 2  ;indent code in src by 2 spaces
        org-hide-block-startup nil          ;don't fold all upon open
        org-src-preserve-indentation nil    ;cleaner src whitespace on export
        org-ellipsis " ⤵" 
        ))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun setup/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
	visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . setup/org-mode-visual-fill))

(setq org-log-done 'time)

(use-package org-roam
      :ensure t
      :hook
      (after-init . org-roam-mode)
      :custom
      (org-roam-directory (file-truename "~/notebook/"))
      :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
               ("C-c n f" . org-roam-find-file)
               ("C-c n g" . org-roam-graph))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))
              (("C-c n I" . org-roam-insert-immediate))))

(use-package helm-bibtex)
(use-package org-roam-bibtex
  :after org-roam
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :config
  (require 'org-ref)) ; optional: if Org Ref is not loaded anywhere else, load it here

(setq reftex-default-bibliography '("~/notebook/bibleography.bib"))
;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/notebook/bib-notes.org"
      org-ref-default-bibliography '("~/notebook/bibleography.bib")
      org-ref-pdf-directory "~/notebook/documents/")

(setq bibtex-completion-bibliography "~/notebook/bibleography.bib"
      bibtex-completion-library-path "~/notebook/documents"
      bibtex-completion-notes-path "~/notebook/bib-notes.org")

(setq org-ref-notes-directory "~/notebook/")
(setq org-ref-notes-function 'orb-edit-notes)

;(quelpa '(org-pomodoro :fetcher github :repo "palkiakerr/org-pomodoro" :files (:defaults "resources")))
(use-package org-pomodoro)

(use-package elegant-agenda-mode
  :hook org-agenda-mode-hook)

(use-package org-timeline)
(add-hook 'org-agenda-finalize-hook 'org-timeline-insert-timeline :append)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (scheme . t)))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-f" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :init
  (ivy-mode 1))
; and make it more descriptive
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0))

;; Counsel alternatives To Common EMACS Commands
(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file))
  :config
  (setq ivy-initial-inputs-alist nil))

;; Replace The Help Help Functions With Better Ones
(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; Set Up DOOM-vibrant Theme
(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-vibrant t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(setup/org-mode-setup)


;; Things to add
(use-package org-noter)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(org-noter yaml-mode which-key visual-fill-column use-package rainbow-delimiters quelpa org-timeline org-roam-bibtex org-pomodoro org-bullets ivy-rich helpful geiser-guile elegant-agenda-mode doom-themes doom-modeline counsel buffer-flip)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
